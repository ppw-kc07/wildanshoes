from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.toHome, name='home'),
    path('kritik/', views.listKritik, name='kritik'),
    # dilanjutkan ...
]