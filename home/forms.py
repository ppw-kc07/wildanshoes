from django import forms
from . import models

class KritikForm(forms.ModelForm):
    saran=forms.CharField(widget=forms.Textarea(attrs={
        "style":"width:1108px; height:200px; resize:none",
        "class":"form-control",
        "required":True,
        "placeholder":"Masukkan saran anda",
    }), label="")
    class Meta:
        model=models.Sarannya
        fields=['saran']