from django.shortcuts import render, redirect
from .forms import KritikForm
from .models import Sarannya
from . import forms
from datetime import datetime, date

def toHome(request):
    if request.method=='POST':
        form=forms.KritikForm(request.POST)
        if form.is_valid():
            saran=form.cleaned_data['saran']
            formSaran = Sarannya(saran=saran)
            formSaran.save()
            #Task.objects.all().delete()
            return redirect('home:home')
        else:
            return render(request, "home.html",{'form':form})
    form=forms.KritikForm()
    return render(request, "home.html",{'form':form})
# Create your views here.

def listKritik(request):
	return render(request, 'ListKritik.html')

def removeKritik(request, saran_id):
    form=Sarannya.objects.get(id=saran_id)
    form.delete()
    return redirect('list')
