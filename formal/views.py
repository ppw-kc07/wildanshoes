from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import FormTanya
from .models import Pertanyaan

# Create your views here.

def tanyakan(request):
    form = FormTanya()
    if request.method =="POST":
        form=FormTanya(request.POST)
        if form.is_valid():
            form.save()
            return redirect('formal:formal')
    return render(request, "kategori2.html", {'form':form})
