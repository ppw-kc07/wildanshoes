from django.db import models

class Pertanyaan(models.Model):
	nama = models.CharField(max_length=100)
	kota = models.CharField(max_length=50)
	isi = models.CharField(max_length=1000)

	def __str__(self):
		return self.nama
