from django.db import models

# Create your models here.

class Description(models.Model):
    dropchoice = (('40','40'),('41','41'),('42','42'),('43','43'),('44','44'))
    ukuran = models.CharField(max_length=20, choices=dropchoice,default='40')
    catatan = models.CharField(max_length=100)
    jumlah = models.IntegerField(default=1)