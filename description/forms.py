from django import forms
from . import models

class DescForms(forms.ModelForm):
    class Meta:
        model = models.Description
        fields = ['ukuran','catatan','jumlah']
