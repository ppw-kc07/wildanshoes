from . import models
from django import forms

class FormTanya(forms.ModelForm):
                nama = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "askfields full",
                "required" : True,
                "placeholder":"Nama Lengkap",
                }))
                kota = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "askfields full",
                "required" : True,
                "placeholder":"Kota",
                }))
                isi = forms.CharField(widget=forms.Textarea(attrs={
                "class" : "askfields full",
                "required" : True,
                "placeholder":"Pertanyaan",
                }))

                class Meta:
                                model = models.Pertanyaan
                                fields = ["nama", "kota", "isi"]