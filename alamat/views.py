from django.shortcuts import render, redirect
from .forms import PostForm
from .models import Form
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy

class AlamatDelete(DeleteView):
        model = Form
        success_url = reverse_lazy('alamat:alamatAdmin')

def alamat(request):
    post = Form.objects.all()
    post_form = PostForm(request.POST or None)


    if request.method == 'POST':
        # if 'id' in request.POST:
        #     Form.objects.get(id=request.POST['id']).delete()
        #     return redirect('jadwal:jadwal')
        
        if post_form.is_valid():
            post_form.save()
            return redirect('alamat:submitted')

    context = {
        'post_form' : post_form,
        'posting': post 
    }
    return render(request, 'alamat.html',context)


def alamatAdmin(request):
    post = Form.objects.all()
    post_form = PostForm(request.POST or None)


    # if request.method == 'POST':
    #     # if 'id' in request.POST:
    #     #     Form.objects.get(id=request.POST['id']).delete()
    #     #     return redirect('jadwal:jadwal')
        
    #     if post_form.is_valid():
    #         post_form.save()
    #         return redirect('alamat:alamatAdmin')

    context = {
        'post_form' : post_form,
        'posting': post 
    }
    return render(request, 'alamatAdmin.html',context)

def submitted(request):
   
    return render(request, 'submitted.html')