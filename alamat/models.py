from django.db import models


class Form(models.Model):
     
    Nama_Pembeli = models.CharField(max_length=50)
    Nama_Produk = models.CharField(max_length=50)
    Nomor_Handphone = models.CharField(max_length=50)
    Nomor_Telepon = models.CharField(max_length=50)
    Alamat_Lengkap = models.TextField(max_length=500)
