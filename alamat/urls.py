from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'alamat'

urlpatterns = [
    path('', views.alamat, name='alamat'),
    path('alamatAdmin/', views.alamatAdmin, name='alamatAdmin'),
    path('submitted/', views.submitted, name='submitted'),
    url(r'(?P<pk>[0-9]+)/delete/$', views.AlamatDelete.as_view(), name='delete'),
]
